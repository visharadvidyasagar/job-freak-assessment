const detail = [
    {
      id: 1,
      title: "Job Freak",
      category: "Home",
      img: "./images/home.png",
      desc: `At Job Freak, we post daily updates on internships and job postings!<br><br>
      Our aim is to empower the youth of our country and make them self-dependent by providing them with jobs/internships and all kinds of opportunities from various sectors.
      
      <br><br><br><br>Hint: *hover on the image*`,
    },
    {
      id: 2,
      title: "Contact Us",
      category: "Contact",
      img: "./images/contact.png",
      desc: `Mobile: +91 81xxxxxx26 <br><br>
      email: jobfreak@abc.com<br><br>
      Location: India`,
    },
    {
      id: 3,
      title: "Information",
      category: "Information",
      img: "./images/info.jpg",
      desc: `About the work from home job/internship<br><br><br>
      Selected intern's day-to-day responsibilities include:<br><br>
      1. Create web pages and multiple functionality<br>
      2. Manage the code documentation<br>
      3. Enhance the features of the web application`,
    },
    {
      id: 4,
      title: "Guide",
      category: "Guide",
      img: "./images/guide.avif",
      desc:`Only those candidates can apply who:<br><br>

      1. Are available for the work from home job/internship<br>
      
      2. Can start the work from home job/internship between 13th Jun'23 and 18th Jul'23<br>
      
      3. Are available for duration of 2 months<br>
      
      4. Have relevant skills and interests<br><br>
      
      * Women wanting to start/restart their career can also apply.`
    },
  ];
  
  
  const content = document.querySelector(".content");  
  const filterBtns = document. querySelectorAll(".filter-btn");    
  
  window.addEventListener("DOMContentLoaded",function(){
    displayDetailItems(detail[1]);
  });
    filterBtns.forEach(function (btn) {  
    btn.addEventListener("click",function(e) {  
       const category =  e.currentTarget.dataset.id;
       const detailCategory = detail.filter(function (detailItem) {
        if (detailItem.category === category){  
          return detailItem;
        }
       });
        if( category === "All"){ 
          displayDetailItems(detail);
        } else {
          displayDetailItems(detailCategory);
        }    
      });
  });
  
  function displayDetailItems (detailItems){
    let displayDetail = detailItems.map(function(item){
      
      return `
        <div class="content">
        <h1>${item.title}</h1>

        <img src=${item.img} alt="">

        <p class="desc">${item.desc}</p>
        </div>
        `;
    });
    displayDetail = displayDetail.join("");
    content.innerHTML = displayDetail;
  }